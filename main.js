const electron = require('electron');

const { app, BrowserWindow } = electron;

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// Grab command line input arguments for later processing
const dev = process.argv[2];

let developmentMode = false;
if (dev === '-d') {
  developmentMode = true;
}

function createWindow() {
  const openWindow = () => {
    // Create the browser window.
    mainWindow = new BrowserWindow({
      width: 1300,
      height: 1000,
      webPreferences: {
        nodeIntegration: true,
      },
    });
    mainWindow.maximize();

    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, './build/index.html'),
      protocol: 'file:',
      slashes: true,
    }), { extraHeaders: 'pragma: no-cache\n' });

    // Open the DevTools if we should - Command line argument
    if (developmentMode) {
      mainWindow.webContents.openDevTools();
    }

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      mainWindow = null;
      // On Windows, we have to manually kill the container application, otherwise
      // we will leave one zombie process running.
    });
  };

  openWindow();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  app.quit();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});
